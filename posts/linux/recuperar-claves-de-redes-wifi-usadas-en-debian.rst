.. title: Recuperar claves de redes WiFi conectadas en Debian
.. slug: claves-wifi-conectadas
.. date: 2013-08-13 19:10:00 UTC-05:00
.. tags: clave, CrunchBang, Debian, red, wifi, NetworkManager
.. category: Linux
.. link: 
.. description: 
.. type: text

No, no me refiero a "auditar" una red. Esta entrada es para obtener las claves de los **access points** a los que ya nos hemos
conectado, puede ser que olvidamos la clave y nos da pereza revisarla directamente en el AP.

La información sobre todas las redes a las que nos conectamos se almacenan como archivos de texto plano y se encuentran en la ruta
*/etc/NetworkManager/system-connections*, los archivos tienen el mismo nombre de la red, solo hace falta editarlos con nuestro editor de texto
favorito.

	Atención: Debes al ser un directorio del sistema debes usar el comando sudo para poder leer el archivo

En mi caso necesito la clave de AP con el nombre de **4 8 15 16 23 42** (soy fan de LOST :P):

..	code-block::

	sudo cat /etc/NetworkManager/system-connections/4\ 8\ 15\ 16\ 23\ 42

Las claves se encuentran en la sección *[wifi-security]*.
