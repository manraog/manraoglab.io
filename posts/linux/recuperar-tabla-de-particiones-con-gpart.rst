.. title: Recuperar tabla de particiones con gpart
.. slug: recuperar-tabla-de-particiones
.. date: 2013-06-14 23:05:00 UTC-05:00
.. tags: Bunsenlabs, Debian, gpart, GParted, GPT, Linux, LVM, MBR, particion, terminal, Windows
.. category: Linux
.. link: 
.. description: 
.. type: text

Antes que nada hay que aclarar que
`GParted <http://gparted.sourceforge.net/>`__ y gpart son completamente
distintos, el primero nos permite editar particiones de forma gráfica
como el conocido Partition Magic y el ultimo, que es el que usaremos,
nos permite detectar particiones que estan en el disco duro pero que por
alguna razón no aparecen en la tabla de particiones MBR.

En mi caso accidentalmente borre la tabla de particiones de mi disco duro 
con GParted y este es el procedimiento que seguí para recuperarlas usando 
el modo live de Debian 7 - Crunchbang (Ahora bunsenlabs).

	El MBR (Master Boot Record) son los primero 512 bytes del disco duro o cualquier otro dispositivo de almacenamiento
	almacenamiento, en este se encuentra el bootloader del SO y la tabla de particiones (como se encuentra organizado el disco y en que formato). 
	Actualmente se busca remplazar por  GPT (GUID Partition Table) debiado asus limitantes como el no poder crear más de de 4 particiones primarias.

--------------------------------
Recuperando tabla de particiones
--------------------------------

Como podemos ver GParted no reconoce las particiones.

|Screenshot - 06142013 - 09:30:27 PM|

Instalamos gpart.

..	code-block:: bash

	sudo aptitude install gpart

Usamos gpart en el dispositivo, tardara un rato según el tamaño de tu
disco.

.. code-block:: bash

	gpart /dev/sda

	| Begin scan...
    |  Possible partition(Windows NT/W2K FS), size(30718mb), offset(0mb)
    |  Possible partition(Windows NT/W2K FS), size(0mb),
    offset(109197mb)
    |  Possible extended partition at offset(420451mb)
    |  Possible partition(Windows NT/W2K FS), size(56485mb),
    offset(420451mb)
    |  End scan.

    | Checking partitions...
    |  Partition(OS/2 HPFS, NTFS, QNX or Advanced UNIX): primary
    |  Partition(OS/2 HPFS, NTFS, QNX or Advanced UNIX): primary
    |  Partition(OS/2 HPFS, NTFS, QNX or Advanced UNIX): primary
    |  Ok.

    | Guessed primary partition table:
    |  Primary partition(1)
    |  type: 007(0x07)(OS/2 HPFS, NTFS, QNX or Advanced UNIX)
    |  size: 30718mb #s(62910477) s(63-62910539)
    |  chs:  (0/1/1)-(1023/254/63)d (0/1/1)-(3915/254/63)r

    | Primary partition(2)
    |  type: 007(0x07)(OS/2 HPFS, NTFS, QNX or Advanced UNIX)
    |  size: 0mb #s(1) s(223636581-223636581)
    |  chs:  (1023/254/63)-(1023/254/63)d (13920/187/1)-(13920/187/1)r

    | Primary partition(3)
    |  type: 007(0x07)(OS/2 HPFS, NTFS, QNX or Advanced UNIX)
    |  size: 56485mb #s(115683057) s(861084063-976767119)
    |  chs:  (1023/254/63)-(1023/254/63)d (53600/1/1)-(60800/239/63)r

    | Primary partition(4)
    |  type: 000(0x00)(unused)
    |  size: 0mb #s(0) s(0-0)
    |  chs:  (0/0/0)-(0/0/0)d (0/0/0)-(0/0/0)r


--------------------------
Grabando el MBR
--------------------------

Si las particiones son correctas procedemos a guardar el MBR en un archivo.

.. code-block:: bash

        gpart -W ruta_mbr /dev/hdb

Y escribir el MBR en el disdo duro.

.. code-block:: bash

        dd if=mbr_del_sistema of=/dev/sda

En mi caso lo escribire directamente en el DD sin guardarlo en un archivo.

.. code-block:: bash

        gpart -W /dev/sda /dev/sda

Al final nos permite editar las particiones.


Ahora GParted muestra las particiones y podemos montarlas.

|Screenshot - 06152013 - 12:19:36 AM|

-------------------------------
Prevenir es mejor que lamentar
-------------------------------

Aunque el proceso fue muy sencillo siempre puede fallar algo (y en el mundo de la informática cuando menos te lo esperas) así que mejor respaldar
la tabla de particiones MBR.

Para respaldar todas las particiones incluyendo las lógicas.

.. code-block:: bash

	sfdisk -d /dev/sda > backup-sda.sf

Y para recuperarlas.

.. code-block:: bash

        sfdisk /dev/sda < backup-sda.sf

|

**Referencias:**

`Master Boot
Record <https://wiki.archlinux.org/index.php/Master_Boot_Record>`__ \|
Wiki de Arch Linux

`gpart <http://en.wikipedia.org/wiki/Gpart>`__ \| Wikipedia

`Recuperacion de particiones perdidas con
gpart <http://www.vicente-navarro.com/blog/2008/07/25/recuperacion-de-particiones-perdidas-con-gpart/>`__
\| Lo hice y lo entendí

| 

.. |Screenshot - 06142013 - 09:30:27 PM| image:: https://manraog.files.wordpress.com/2013/06/screenshot-06142013-093027-pm.png
   :target: http://manraog.files.wordpress.com/2013/06/screenshot-06142013-093027-pm.png
.. |Screenshot - 06152013 - 12:19:36 AM| image:: https://manraog.files.wordpress.com/2013/06/screenshot-06152013-121936-am.png?w=710
   :target: http://manraog.files.wordpress.com/2013/06/screenshot-06152013-121936-am.png
