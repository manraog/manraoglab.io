.. title: Montar particiones NTFS/FAT32 permanentes
.. slug: montar-particiones-ntfsfat32-permanentes
.. date: 2013-06-15 19:32:00 UTC-05:00
.. tags: FAT32, fstab, Linux, LVM, NTFS, particion, USB, Windows
.. category: Linux
.. link: 
.. description: 
.. type: text

Actualmente los sistemas Windows usan
`NTFS <http://en.wikipedia.org/wiki/NTFS>`__, como remplazo al ya viejo `FAT32 <http://en.wikipedia.org/wiki/File_Allocation_Table>`__, aunque aún 
se sigue usando `en ciertos casos <http://community.us.playstation.com/t5/PlayStation-3/External-Harddrive-for-PS3-only-FAT32/td-p/14726163>`__, 
de cualquier forma aprenderemos como acceder a nuestros archivos en ambos sistemas de archivos. Para realizar el proceso con NTFS es necesario instalar el 
paquete `ntfs-3g <http://en.wikipedia.org/wiki/NTFS-3G>`__ que nos permite usar este tipo de partición, finalmente para montar permanentemente ambos sistemas 
editaremos el archivo *fstab.*

	En el archivo fstab (File System Table) se encuentra la lista de los
	sistemas de ficheros a montar en el arranque, en el podemos
	encontrar los puntos de montaje de discos duros, particiones, CDs/DVDs y
	memorias USB.

Creamos la carpeta donde montaremos nuestra partición puede ser en
*/mnt* o en */media* en mi caso usare ambas para montar una partición en
cada una.

..	code-block:: bash
   
	sudo mkdir /mnt/windows
	sudo mkdir /media/multimedia

Instalamos ntfs-3g.

..	code-block:: bash
   
	sudo aptitude install ntfs-3g


Buscamos discos *NTFS* o *FAT 32* según sea el caso.

..	code-block:: bash

	sudo fdisk -l | grep NTFS

En mi caso obtengo lo siguiente, las cuatro primeras lineas son extrañas debido a que
están en
`LVM <http://en.wikipedia.org/wiki/Logical_Volume_Manager_%28Linux%29>`__
pero las ultimas dos lineas muestran mis particiones en NTFS

.. code-block::

	Disk /dev/mapper/crunchbang-root doesn’t contain a valid partition table
	Disk /dev/mapper/crunchbang-swap doesn’t contain a valid partition table
	Disk /dev/mapper/crunchbang-temp doesn’t contain a valid partition table
	Disk /dev/mapper/crunchbang-home doesn’t contain a valid partition table
	/dev/sda1   *          63    62910539    31455238+   7  HPFS/NTFS/exFAT
	/dev/sda3       861084063   976767119    57841528+   7  HPFS/NTFS/exFAT

Lo único que nos importa es la "ruta" de nuestra partición, en este caso
*/dev/sda1* y */dev/sda3.*

Abrimos el archivo fstab con nuestro edito favorito, en mi caso nano.

..	code-block:: bash
   
	sudo nano /etc/fstab

Y agregamos la siguiente linea, remplazar las rutas por las requeridas
en tu sistemas. En caso de montar FAT32 usar *vfat* en lugar de
*ntfs-3g.*

.. code-block::

	/dev/sda1 /mnt/windows ntfs-3g auto,rw,users,umask=000 0 0
	/dev/sda3 /media/multimedia ntfs-3g auto,rw,users,umask=000 0 0

Ejecutamos el siguiente comando para montar todas las particiones en
fstab

..	code-block:: bash

	sudo mount -a

Solo nos resta acceder a las particiones desde la ruta dónde las
montamos en este caso */media/multimedia* y */mnt/windows*

|
	
**Referencias:**

`Lo esencial del archivo
fstab <http://blog.desdelinux.net/wp-content/uploads/2012/02/Lo_esencial_de_fstab.pdf>`__
\| HumanOS pero parece ya no estar disponible, esta es una copia
encontrada en `DesdeLinux <http://blog.desdelinux.net/>`__
