.. title: Mostrar progreso del comando dd
.. slug: mostrar-progreso-comando-dd
.. date: 2013-08-25 23:08:00 UTC-05:00
.. tags: dd, pipe, pv, Raspberry, Linux
.. category: Linux
.. link: 
.. description: 
.. type: text

El comando *dd* (Dataset Definition) es uno de los más útiles y sencillos, aunque también es bastante peligroso (si se usa una ruta
equivocada adiós a tus datos). Nos permite copiar y grabar información, principalmente se usa para crear live USBs o en la  Raspberry Pi
para grabar imágenes de distribuciones en la SD, para que usar una herramienta tan pesada (no soy fan de Electron) como Etcher para algo tan simple. 
Aunque si estas aquí es por que probablemente ya lo has usado.

La desventaja de este comando es que no muestra información sobre el progreso, a veces da la impresión de que se ha trabado. Para solucionar esto 
existen varias formas de ver el progreso de este comando.

-------------------
Enviando señal USR1
-------------------

Este método es muy útil cuando ya se esta ejecutando *dd*, te da la
impresión de que se a congelado y quieres conocer el progreso. A demás
no requiere instalar ningún paquete extra.

Primero necesitamos conocer el ID del comando dd en ejecución.

..	code-block:: bash	
	
	pgrep -l '^dd'

En mi caso me devuelve

..	code-block:: bash
    
    24701 dd

Ahora enviaremos la señal USR1 al proceso de *dd* para que nos muestre el
estado.

..	code-block:: bash

	kill -USR1 24701

Y no solo eso, si queremos que se ejecute periódicamente para tener un
monitoreo constante podemos usar *watch*.

..	code-block:: bash
	
	watch -n 10 kill -USR1 24701


----------------------
Usando la "tubería" PV
----------------------

Aquí usamos el comando *pv* que es una tubería para monitoriza la
información que pasa a través de ella. Existen muchas formas de usar
*pv*, pero la que me parece más sencilla y útil para trabajar con *dd*
es:

..	code-block::
	
	pv /ruta-entrada | dd bs=1M of=/ruta-salida

Recuerda que si no tienes instalado pv puedes hacerlo desde tu
repositorio.

pv nos muestra la cantidad copiada, el tiempo transcurrido, el tiempo
restante y una bonita barra de progreso

.. code-block::
    
    2.86GB 0:14:48 [3.47MB/s] [=============>                      ] 41%
    ETA 0:20:33

Si te interesa profundizar más en el uso del comando *pv* puedes
consultar `este
enlace <http://blog.allanglesit.com/2011/08/bash-using-pv-to-display-progress-of-dd/>`__.

|

**Referencias:**

`Show progress during dd
copy <http://linuxcommando.blogspot.mx/2008/06/show-progress-during-dd-copy.html>`__
\| Linux commando

`Using pv to display progress of
dd <http://blog.allanglesit.com/2011/08/bash-using-pv-to-display-progress-of-dd/>`__
\|IT from all angels
