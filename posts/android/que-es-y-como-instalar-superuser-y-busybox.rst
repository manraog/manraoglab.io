.. title: ¿Qué son Superuser y Busybox?
.. slug: que-son-superuser-y-busybox
.. date: 2013-08-15 15:00:00 UTC-05:00
.. tags: Android, Busybox, Superuser, root
.. category: Android
.. link: 
.. description: 
.. type: text

------------------
¿Qué es Superuser?
------------------

|superuser-logo|

    En entornos Linux el super usuario también se conoce como root, este
    usuario puede acceder a todo el sistema y modificarlo sin ningún
    tipo de restricción.

Hay que recordar que Android posee el **kernel Linux** y hay muchas similitudes en la forma de usarlo respecto a cualquier distribución como Debian o 
Fedora. Por defecto no poseemos los privilegios para modificar todos los archivos, esto como medida de seguridad, si cualquier usuario puede acceder a
las "tripas" del SO modificaría archivos vitales para su correcto funcionamiento.

    En sistemas Linux el comando *su* nos permite cambiar de usuario sin
    cerrar nuestra sesión actual. Puedo estar en mi sesión de usuario
    sin privilegios y usar *su* en una terminal para acceder como root a
    los archivos del sistema, que no podría abrir con mi usuario normal.

Superuser permite administrar las aplicaciones a las que permitimos el
uso del comando **su** para ejecutar acciones como root. Se compone
del **.apk** o la aplicación como tal con su interfaz gráfica y un
**binario** al que las aplicaciones le piden el acceso al comando **su** y
se los permite o no según segun nuestra lista. Así no se ejecuta **malaware** 
a menos que nosotros le permitiéramos escalar privilegios con **su**.

Instalación
-----------

Normalmente
`Superuser <https://play.google.com/store/apps/details?id=com.noshufou.android.su&hl=es>`__
o `Super
SU <https://play.google.com/store/apps/details?id=eu.chainfire.supersu&hl=es>`__
que es otra aplicación con el mismo fin, se instalan al momento de
rootear el dispositivo, esto ya depende del modelo y el metodo usado.
Algunos dispositivos están rooteados de fabrica, para saber si el
nuestro lo esta; buscamos la aplicación Superuser o Super Su en nuestro
dispositivo, si no se encuentra podemos descargar `Root
Checker <https://play.google.com/store/apps/details?id=org.freeandroidtools.root_checker&hl=es>`__
de Google Play o cualquier otra market.

En el caso particular de la Skytab SP722 no me funciono Super SU y el
Superuser solo el descargado desde 1Mobile Market.

Para actualizar el binario hay que pulsar en el icono:

|Screenshot-1|

Y después aquí:

|Screenshot-2|

Con esto Superuser buscara si hay alguna actualización y la podremos
instalar.

----------------
¿Que es Busybox?
----------------

|image3|

Busybox fue creado originalmente por Bruce Perens para tener un sistema
Debian funcional en un disco flexible (sí, en tan limitado espacio), eso 
hace a Busybox perfecto para discos de recuperación o sistemas embebidos.

Busybox reúne muchas herramientas de sistemas tipo UNIX en un solo
binario o ejecutable, al instalarlo en Android podemos ejecutar una
amplia gama de comandos como en nuestra distribución Linux favorita.
Muchas aplicaciones hacen uso de las utilidades de Busybox como Titanium
Backup o los emuladores de Terminal.

Instalación
-----------

En dispositivos con ROMs cocinadas es común que Busybox ya se encuentre
instalado. En ROMs stock es necesario que el dispositivo este rooteado y
usar una aplicación para permitir elevar privilegios como superuser. En
varios sitio recomiendan usar `Busybox de Stephen
(Stericson) <https://play.google.com/store/apps/details?id=stericson.busybox>`__
para instalarlo, pero no te permite elegir el directorio dónde lo hará.
Esto es un problema si lo que quieres es actualizar el Busybox que ya se
encontraba en tu ROM, por defecto la aplicación de Stephen lo instala en
*/system/xbin/ *\ pero hay ROMs que lo tienen en */system/bin/.*

En el caso de la Skypad SP722 al instalarlos en *xbin* si hacía un
apagado completo de la tablet, al encenderla restauraba el sistema a un
Android "puro" no dejaba ni las modificaciones de Skytex. Esto es un
problema enorme, por su baja batería es necesario estar apagando
constantemente la tablet, pero no podía hacerlo ya perdía toda mi
configuración y aplicaciones en el siguiente encendido.

Mi recomendación es usar `**Busybox installer de JRummy
apps**, <https://play.google.com/store/apps/details?id=com.jrummy.busybox.installer>`__
a demás de indicarte la ruta actual de tu Busybox te deja elegir la ruta
dónde se instalara el nuevo y no solo eso, también puedes elegir
versiones anteriores de Busybox, aunque es altamente recomendable usar
la ultima.

Una vez descargado Busybox installer desde Google Play o cualquier otra
Market procedemos a la instalación:

|Screenshot-3|

Revisamos en dónde se encuentra instalado nuestro actual Busybox y
cambiamos la ruta de instalación por la de nuestro Busybox. En mi caso
cambiare de *xbin* a *bin.*

Si usaremos aplicaciones que requieren más herramientas de Busybox como
`DSploit <http://www.dsploit.net/>`__ marcamos la casilla de *Advanced
install* y revisamos que todos los comandos se encuentran marcados y
procedemos a instalar.

**Referencias:**

`Superuser <http://androidsu.com/superuser/>`__ \| Superuser web

`¿Cuál es la diferencia entre sudo y
su? <http://blog.desdelinux.net/cual-es-la-diferencia-entre-sudo-y-su/>`__
\| Blog DesdeLinux

`Busybox <http://packages.debian.org/sid/busybox>`__ \| Debian Packages

`What is Busybox for an Android
user? <https://chislonchow.wordpress.com/2013/03/29/what-is-busybox-for-an-android-user/>`__
\| Chislon Chow's Blog

.. |superuser-logo| image:: https://manraog.files.wordpress.com/2013/08/superuser-logo.png
   :target: https://manraog.files.wordpress.com/2013/08/superuser-logo.png
.. |Screenshot-1| image:: https://manraog.files.wordpress.com/2013/08/screenshot_2013-08-14-21-19-30.png?w=710
   :target: https://manraog.files.wordpress.com/2013/08/screenshot_2013-08-14-21-19-30.png
.. |Screenshot-2| image:: https://manraog.files.wordpress.com/2013/08/screenshot_2013-08-14-21-19-37.png?w=710
   :target: https://manraog.files.wordpress.com/2013/08/screenshot_2013-08-14-21-19-37.png
.. |image3| image:: https://lh5.ggpht.com/F8TJh9q7wE9noPJNzRjzCrOSJw6_fyVoEZYE9xl01IO4qg9mkAOv29DV9REZOmlR7JM=w300
.. |Screenshot-3| image:: https://manraog.files.wordpress.com/2013/08/screenshot_2013-08-14-14-05-19.png?w=710
   :target: https://manraog.files.wordpress.com/2013/08/screenshot_2013-08-14-14-05-19.png
