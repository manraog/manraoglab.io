.. title: Hello world... Again
.. slug: hello-world-again
.. date: 2017-07-19 00:23:59 UTC-05:00
.. tags: Blog
.. category: Blog 
.. link: 
.. description: 
.. type: text

El blog cobra vida en `Gitlab <http://gitlab.com/>`__ con `Nikola <https://getnikola.com/>`__, aunque aún faltan algunos detalles el blog ya esta funcional.

GitLab pages me pareció mucho mejor opción frente a GitHub pages y con la opción de `GitLab CI <https://about.gitlab.com/features/gitlab-ci-cd/>`__ puedo publicar desde cualquier equipos sin necesidad de tener instalado
Nikola, incluso puedo hacerlo con un commit desde la web de Gitlab y sus *shared runners* se encargan de construir el blog.

Conservaré el `antiguo blog como recuerdo/archivo <https://manraog.wordpress.com/>`__ así que ya no se actualizara.
