.. title: Proyectos

----------------------
Neuralog
----------------------

**2017 | Tensorflow, Python, Prolog y Arduino**

.. media:: https://www.youtube.com/watch?v=L-YZDdAM1cM

Domótica y control por voz con una red neuronal. Utiliza Prolog para llevar el registro de los objetos (base de conocimiento), modificar su estado 
y ubicación en la casa. La interfaz esta en PyQT4, el asistente de voz lista los objetos en el cuarto según la ubicación de la persona en la casa (sensores 
infrarrojos) y la red neuronal reconoce las palabras *sí* y *no*, si la palabra es *sí* cambia de estado el objeto (encender/apagar) y si la palabra es *no* 
deja el objeto sin cambios.

`Código en GitHub <https://github.com/manraog/Neuralog>`__

------------------------------------------------------------------------------------------------------------------------------------------

-------------------------
Robot resuelve laberintos
-------------------------

**2017 | Arduino, motores, sensor ultrasónico e infrarrojos CNY70**

.. media:: https://www.youtube.com/watch?v=Fj559EWrD3Q

Utilizando una función recursiva puede resolver un laberinto con lineas rectas y bifurcaciones en ambos sentidos de casi cualquier tamaño (hasta que 
el *heap* aguante) con un programa bastante pequeño.

`Código en GitHub <https://github.com/manraog/robot-resuelve-laberintos>`__

------------------------------------------------------------------------------------------------------------------------------------------

----------
ServoPlate
----------

**2016 | TI MSP430, MPU6050 y servo motores**

.. media:: https://www.youtube.com/watch?v=kQwWf2_LYjU

Plataforma que replica el movimiento de un acelerómetro. La idea original era un PID para mantener el equilibrio pero no se pudo implementar 
por cuestiones de tiempo y un hardware tan limitado. Proyecto para la materia de *microcontroladores.*

`Código en GitHub <https://github.com/manraog/servoplate>`__

------------------------------------------------------------------------------------------------------------------------------------------

-------
SpeakPy 
-------

**2014 | Raspberry Pi, Python y Shell scripting**

.. media:: https://www.youtube.com/watch?v=NlDhiJjKRc8&t

Asistente por voz. Lo utilicé en un pequeño proyecto de domótica para la materia de *circuitos eléctricos.*

`Código en GitHub <https://github.com/manraog/SpeakPy>`__

------------------------------------------------------------------------------------------------------------------------------------------

------
CAVAAC
------

**2013 | Ruby on Rails, Bootstrap, HTML y CSS**

Aplicación web para calcular cilindros neumáticos se desarrollo para la materia de *ingeniería de software.*

.. image:: /proyectos/cavaac.png

`Demo en Heroku <http://cavaac.herokuapp.com/>`__

`Código en GitHub <https://github.com/manraog/CAVAAC>`__
