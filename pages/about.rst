.. title: Sobre mí

----------------------
¿Quien soy?
----------------------

Estudiante de Ingeniería en `Telecomunicaciones,  Sistemas y Electrónica <http://www.cuautitlan.unam.mx/licenciaturas/itse/index.html>`__, 
especialización en Sistemas Digitales en la `UNAM - Facultad de Estudios Superiores Cuautitlán <http://www.cuautitlan.unam.mx/index.html>`__,
interesado en programación y GNU/Linux. Melómano  y cinéfilo en mi tiempo libre.

Conociendo la tecnología
------------------------

Desde niño la tecnología siempre llamo mi atención y afortunadamente tuve mi primer contacto con la computadora de mi papá los 5 años, aunque en esos
tiempos solo la usaba para jugar "El Rey León", "Winnie Pooh" o aprendía "matemáticas con Pipo".

Cuando tuve una conexión Dial-up 56Kbs cambio mi forma de ver el mundo, era como tener una biblioteca inmensa (me sentía en la Matrix :P). 
Eventualmente conocí Ubuntu a los 13 años gracias a la moda de Compiz, pero no fue hasta los 16 que use Debian como mi SO principal. 
También por esos años aprendí a programar en C de forma autodicta ya que el profesor se limitaba a dictar de un libro con un lenguaje muy técnico, 
incluso usabamos un compilador viejisimo de Borland.


-----------------------------
¿Que significa ~$ man raog?
-----------------------------

En la terminal el símblo "~$" significa que puedes escribir.

En sistemas tipo Unix el comando man se utiliza para consultar el manual de un comando. En el se encuentra la descripción, opciones y ejemplos de uso del comando.

Ahora resulta sencillo entender el nombre.

---------------------------------------------------

**Distribución GNU/Linux:** `Bunsenlabs <https://www.bunsenlabs.org/>`__ (basada en `Debian <http://www.debian.org>`__).

**Banda favorita:** `Tool <http://youtu.be/VdmLGrZ-Nuk>`__.

**e-mail:** raog(a)protonmail.com
